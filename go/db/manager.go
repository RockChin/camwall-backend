package db

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

func init() {
	db, err := sql.Open("mysql", "camwall:000112@tcp(camwall.idoknow.top:3306)/camwall")

	if err != nil {
		panic(err)
	}

	DB = db

}
